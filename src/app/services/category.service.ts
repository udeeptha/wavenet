import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import categories from '../api/categories/categories.json';
import { ICategory } from '../interfaces/category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private subject = new Subject<any>();

  constructor() { 
    
  }

  getCategory() : ICategory[]{
    return categories;
  }

  sendCatData(catId: number,category: string) {
    this.subject.next({ id: catId , name:category});
  }

  getCatID(): Observable<any> {
      return this.subject.asObservable();
  }

  getCatIdByCategory(category:string): number {
    return categories.find(c => {c.name === category}).id;
  }
}
