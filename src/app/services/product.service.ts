import { Injectable } from '@angular/core';
import products from '../api/products/prodcuts.json';
import { IProduct } from '../interfaces/product';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private subject = new Subject<IProduct[]>();

  constructor() { 
    this.subject.next(products);
  }

  getProducts():IProduct[] {
    return products;
  }

  getProductsById(id:number):IProduct {
    return products.find(p => p.id === id);
  }

  getProductsByCatId(catId:number):IProduct[] {
    return products.filter(p => p.catId === catId);
  }
}
