export interface IProduct {
    id: number;
    name: string;
    code: string;
    available: string;
    price: number;
    catId: number;
    imageUrl: string;
    images: any[]
}
