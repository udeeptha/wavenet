import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';
import { IProduct } from 'src/app/interfaces/product';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  constructor(private route : ActivatedRoute, private router : Router, private productService : ProductService) { }

  product: IProduct;

  ngOnInit(): void {
    let id = +this.route.snapshot.paramMap.get("id");
    this.product = this.productService.getProductsById(id);
  }

  onBack() : void {
    this.router.navigate(['']);
  }

}
