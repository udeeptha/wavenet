import { Component, OnInit, Input } from '@angular/core';
import { IProduct } from '../interfaces/product';
import { ProductService } from '../services/product.service';
import { CategoryService } from '../services/category.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products:IProduct[];
  p: number = 1;

  filteredProducts:IProduct[];

  private _listFilter: string;
  public get listFilter(): string {
    return this._listFilter;
  }
  public set listFilter(value: string) {
    this._listFilter = value;
    this.filteredProducts = this.listFilter ? this.performFilter(this.listFilter) : this.products;
  }

  imageWidth:number = 75
  imageMargin:number = 2

  private _category: string;
  public get category(): string {
    return this._category;
  }
  public set category(value: string) {
    this._category = value;
  }

  private _catId: number;
  public get catId(): number {
    return this._catId;
  }
  public set catId(value: number) {
    this._catId = value;
  }

  constructor(private productService : ProductService, private categoryService : CategoryService) { 
    this.catId = categoryService.getCategory()[0]["id"];
    this.category = categoryService.getCategory()[0]["name"];
    this.filteredProducts = this.products;
    this.listFilter = '';
  }

  ngOnInit(): void {
    this.categoryService.getCatID().subscribe({
      next:data => {
        this.catId = data["id"];
        this.category = data["name"];
        this.products = this.productService.getProductsByCatId(this.catId);
        this.filteredProducts = this.products.filter(p => p.catId === this.catId);
      }
    });
    this.products = this.productService.getProductsByCatId(this.catId);
    this.filteredProducts = this.products.filter(p => p.catId === this.catId);
  }

  performFilter(filterBy:string) : IProduct[] {
    filterBy = filterBy.toLocaleLowerCase();
    return this.products.filter((product : IProduct) => 
      product.name.toLocaleLowerCase().indexOf(filterBy) !== -1 || product.code.toLocaleLowerCase().indexOf(filterBy) !== -1
    );
  }

}
