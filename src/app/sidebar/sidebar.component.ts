import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../services/category.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  categories:any[];

  activeProjectIndex: number;

  projectName:string = "Bloomy's Time";

  constructor(private categoryService : CategoryService) { }

  ngOnInit(): void {
    this.categories = this.categoryService.getCategory();
    this.activeProject(0);
  }

  sendMessage(id:number,category:string): void {
    this.categoryService.sendCatData(id,category);
  }

  public activeProject(index: number): void {
    this.activeProjectIndex = index;
  }

}
