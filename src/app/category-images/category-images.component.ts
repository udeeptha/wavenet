import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../services/category.service';
import { IProduct } from '../interfaces/product';
import { ProductService } from '../services/product.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ICategory } from '../interfaces/category';

@Component({
  selector: 'app-category-images',
  templateUrl: './category-images.component.html',
  styleUrls: ['./category-images.component.css']
})
export class CategoryImagesComponent implements OnInit {

  products:IProduct[];
  categories:ICategory[];
  images: any[] = [];
  p:number = 1;

  filteredProducts:IProduct[];

  private _listFilter: string;
  public get listFilter(): string {
    return this._listFilter;
  }
  public set listFilter(value: string) {
    this._listFilter = value;
    this.filteredProducts = this.listFilter ? this.performFilter(this.listFilter) : this.products;
  }

  constructor(private route : ActivatedRoute, private router : Router,private categoryService : CategoryService , private productService : ProductService) { }

  ngOnInit(): void {
    this.categories = this.categoryService.getCategory();
    this.products = this.productService.getProducts();

    this.filterCategory(null);
  }

  onBack() : void {
    this.router.navigate(['']);
  }

  performFilter(filterBy:string) : IProduct[] {
    filterBy = filterBy.toLocaleLowerCase();
    return this.products.filter((product : IProduct) => 
      product.name.toLocaleLowerCase().indexOf(filterBy) !== -1 || product.code.toLocaleLowerCase().indexOf(filterBy) !== -1
    );
  }

  getProductImages(products : IProduct[]): any[] {
    let productImages:any[] = [];
    products.forEach(p => {
      p.images.forEach(image => {
        productImages.push(image);
      })
    });
    return productImages;
  }

  filterCategory(filterVal:any) {
    let catId;
    if (filterVal == "0" || filterVal == null) {
      this.images = this.getProductImages(this.products);
    }
    else {
      catId = +filterVal;
      this.images = this.getProductImages(this.products.filter(p => p.catId === catId));
    }
  }
}
