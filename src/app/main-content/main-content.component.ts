import { Component, OnInit, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { CategoryService } from '../services/category.service';
import { Subscription, Subject } from 'rxjs';

@Component({
  selector: 'app-main-content',
  templateUrl: './main-content.component.html',
  styleUrls: ['./main-content.component.css']
})
export class MainContentComponent implements OnInit,OnDestroy  {

  category:string = 'category';

  catId:number = 1;

  messages: any;
  subscription: Subscription;

  private readonly onDestroy = new Subject<void>();

  constructor(private categoryService : CategoryService) { 
    
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngOnInit(): void {
   
  }

}
