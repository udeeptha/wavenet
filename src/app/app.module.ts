import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {NgxPaginationModule} from 'ngx-pagination';

import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MainContentComponent } from './main-content/main-content.component';
import { ProductDetailsComponent } from './products/product-details/product-details.component';
import { HomeComponent } from './home/home.component';
import { ProductImagesComponent } from './product-images/product-images.component';
import { CategoryImagesComponent } from './category-images/category-images.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    NavbarComponent,
    SidebarComponent,
    MainContentComponent,
    ProductDetailsComponent,
    HomeComponent,
    ProductImagesComponent,
    CategoryImagesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgxPaginationModule,
    RouterModule.forRoot([
      {path:'products/:id',component:ProductDetailsComponent},
      {path: 'home', component: HomeComponent},
      {path: 'productImages/:id', component: ProductImagesComponent},
      {path: 'categoryImages', component: CategoryImagesComponent},
      {path: '', redirectTo: 'home', pathMatch: 'full'}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
