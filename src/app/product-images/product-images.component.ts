import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IProduct } from '../interfaces/product';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-product-images',
  templateUrl: './product-images.component.html',
  styleUrls: ['./product-images.component.css']
})
export class ProductImagesComponent implements OnInit {

  productImages: any[];
  product:string;
  p:number = 1;
  id:number
  
  constructor(private route : ActivatedRoute, private router : Router, private productService : ProductService) { }

  ngOnInit(): void {
    this.id = +this.route.snapshot.paramMap.get("id");
    this.productImages = this.productService.getProductsById(this.id).images;
    this.product = this.productService.getProductsById(this.id).name;
    console.log(this.productImages);
  }

  onBack() : void {
    this.router.navigate(['products',this.id]);
  }

}
